function makePromise1() {
    const myPromise1 = new Promise(function(resolve, reject) {
        const word = document.getElementById("ex1-text").value;
        resolve(word);
    });
    return myPromise1;
}

function executePromise1() {
    const myPromise1 = makePromise1();
    myPromise1.then((message) => {
        console.log("message info: " + message);
    }).catch((message) => {
        console.log(message);
    });
}



function makePromise2() {
    const myPromise2 = new Promise(function(resolve, reject) {
        const word = document.getElementById("ex2-text");

        if (word.value === "good") {
            resolve("ok");
        } else {
            reject("the message is incorrect");
        }
    });

    return myPromise2;
}

function executePromise2() {
    const myPromise2 = makePromise2();
    myPromise2.then((message) => {
        console.log(`message info ${message}`);
    }).catch((message) => {
        console.log(message);
    });
}



function makePromise3() {
    const myPromise3 = new Promise(function(resolve, reject) {
        const number1 = parseInt(document.getElementById("ex3-number1").value);
        const number2 = parseInt(document.getElementById("ex3-number2").value);
        if (number1 === undefined || number2 === undefined) {
            reject("wrong input")
        } else if (number1 > number2) {
            resolve("First number is bigger");
        } else if (number2 > number1) {
            resolve("Second number is bigger");
        } else {
            reject("wrong input")
        }
    });

    return myPromise3;
}

function executePromise3() {
    const myPromise3 = makePromise3();
    myPromise3.then((message) => {
        console.log(message);
    }).catch((message) => {
        console.log(message);
    });
}


function callAllPromises() {
    changeHTMLvalues("My name is Daniel", "good", 1, 2);

    const promises = getProcesses(213);

    promises.then((messeges) => {
        console.log(messeges[0].value)
        console.log(messeges[1].value)
        console.log(messeges[2].value)
    }).catch((messeges) => {
        console.log(messeges);
    })
}

function changeHTMLvalues(value1, value2, value3, value4) {
    document.getElementById("ex1-text").value = value1;
    document.getElementById("ex2-text").value = value2;
    document.getElementById("ex3-number1").value = value3;
    document.getElementById("ex3-number2").value = value4;
}

function getProcesses(order) {
    const myPromise1 = makePromise1();
    const myPromise2 = makePromise2();
    const myPromise3 = makePromise3();

    let promises;

    switch (order) {
        case 123:
            promises = Promise.allSettled([
                myPromise1,
                myPromise2,
                myPromise3
            ]);
            break;
        case 132:
            promises = Promise.allSettled([
                myPromise1,
                myPromise3,
                myPromise2
            ]);
            break;
        case 321:
            promises = Promise.allSettled([
                myPromise3,
                myPromise2,
                myPromise1
            ]);
            break;
        case 312:
            promises = Promise.allSettled([
                myPromise3,
                myPromise1,
                myPromise2
            ]);
            break;
        case 213:
            promises = Promise.allSettled([
                myPromise2,
                myPromise1,
                myPromise3
            ]);
            break;
        case 231:
            promises = Promise.allSettled([
                myPromise2,
                myPromise3,
                myPromise1
            ]);
            break;

        default:
            break;
    }

    return promises;
}

function failEx2AndEx3() {
    changeHTMLvalues("My name is Daniel", "bad", null, 2);

    const promises = getProcesses(312);

    promises.then((messeges) => {
        console.log(messeges[0].reason)
        console.log(messeges[1].value)
        console.log(messeges[2].reason)
    }).catch((messeges) => {
        console.log(messeges);
    })
}

function ex6() {
    changeHTMLvalues("My name is Daniel", "good", 1, 2);

    const promises = getProcesses(123);

    promises.then((messeges) => {
        console.log(messeges[0].value)
        console.log(messeges[1].value)
        console.log(messeges[2].value)
    }).catch((messeges) => {
        console.log(messeges);
    })
}



async function failEx2AndEx3Await() {
    changeHTMLvalues("My name is Daniel", "bad", 1, 2);
    const promise1 = await makePromise1();
    console.log(promise1);
    try {
        const promise2 = await makePromise2();
        console.log(promise2);
    } catch (error) {
        console.log(error)
    }
    const promise3 = await makePromise3();
    console.log(promise3);
}